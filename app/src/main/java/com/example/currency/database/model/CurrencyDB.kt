package com.example.currency.database.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class CurrencyDB constructor(
        @PrimaryKey var currency: String = "",
        var rate: Double? = 0.0
) : RealmObject()
