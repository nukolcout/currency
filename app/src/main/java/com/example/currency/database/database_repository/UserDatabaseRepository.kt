package com.example.currency.database.database_repository

import com.example.currency.database.model.CurrencyDB
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.toObservable
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.where


class UserDatabaseRepository : BaseRepository<CurrencyDB>() {
    val realm: Realm = Realm.getDefaultInstance()

    override fun insert(item: CurrencyDB): String {
        realm.executeTransactionAsync { realm ->
            realm.insert(item)
        }
        return item.currency
    }

    override fun insertList(items: List<CurrencyDB>): List<String> {
        val listIds = mutableListOf<String>()
        realm.executeTransactionAsync {
            realm.insert(items)
            items.forEach {
                listIds.add(it.currency)
            }
        }
        return listIds
    }

    override fun isEmpty(): Boolean {
        val items = realm.where<CurrencyDB>().findAll()
        return items.isEmpty()
    }

    override fun getItemById(itemId: String): CurrencyDB? {
        return realm.where<CurrencyDB>().equalTo("currency", itemId).findFirst()

    }

    override fun getAll(): List<CurrencyDB> {
        return realm.where<CurrencyDB>().findAllAsync()
    }

     fun getAll1(): Observable<CurrencyDB> {
        return realm.where<CurrencyDB>().findAllAsync().toObservable<CurrencyDB>().filter({ it?.isLoaded })
    }

    override fun deleteById(itemId: String) {
        realm.executeTransactionAsync { realm ->
            val food = realm.where<CurrencyDB>().equalTo("currency", itemId).findFirst()
            food?.deleteFromRealm()
        }
    }

    override fun delete(item: CurrencyDB): Single<String> {
        return Single.create { emitter ->
            try {
                item.deleteFromRealm()
                emitter.onSuccess(item.currency)
            } catch (ex: Throwable) {
                emitter.onError(ex)
            }
        }
    }

    override fun update(item: CurrencyDB): Single<CurrencyDB> {
        return Single.create { emitter ->
            try {
                realm.insertOrUpdate(item)
                emitter.onSuccess(item)
            } catch (ex: Throwable) {
                emitter.onError(ex)
            }
        }
    }
}