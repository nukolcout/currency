package com.example.currency.database.database_repository

import io.reactivex.Single

abstract class BaseRepository<T> {
    abstract fun insert(item: T): String
    abstract fun insertList(items: List<T>): List<String>
    abstract fun isEmpty(): Boolean
    abstract fun getItemById(itemId: String): T?
    abstract fun getAll(): List<T>
    abstract fun deleteById(itemId: String)
    abstract fun delete(item: T): Single<String>
    abstract fun update(item: T): Single<T>
}