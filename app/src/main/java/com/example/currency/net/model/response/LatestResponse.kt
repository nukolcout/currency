package com.example.currency.net.model.response


import com.example.currency.ui.base_adapter.CurrencyUI
import com.google.gson.annotations.SerializedName

data class LatestResponse(
    @SerializedName("base")
    val base: String,
    @SerializedName("disclaimer")
    val disclaimer: String,
    @SerializedName("license")
    val license: String,
    @SerializedName("rates")
    val rates: LinkedHashMap<String, Double>,
    @SerializedName("timestamp")
    val timestamp: Int,
    val select: Boolean = false
) {
    fun <K : String, V : Double> LinkedHashMap<K, V>.toCurrencyUi(): ArrayList<CurrencyUI> {
        val currencyUIs = arrayListOf<CurrencyUI>()
        for (item in this) {
            val currencyUI = CurrencyUI(currency = item.key, rate = item.value)
            currencyUIs.add(currencyUI)
        }
        return currencyUIs
    }
}