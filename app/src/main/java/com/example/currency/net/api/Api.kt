package com.example.currency.net.api

import com.example.currency.net.model.response.LatestResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface Api {
    @GET("/latest.json?app_id=2850201a06144d9fb2c7cd655a11f89b")
    fun getRepositories(): Observable<Response<LatestResponse>>
}