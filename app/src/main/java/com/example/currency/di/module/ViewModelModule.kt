package com.example.currency.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.currency.di.ViewModelFactory
import com.example.currency.di.ViewModelKey
import com.example.currency.ui.currency.CurrencyVM
import com.example.currency.ui.profile.ProfileVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ProfileVM::class)
    fun bindGitHubProjectVM(viewModel: ProfileVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CurrencyVM::class)
    fun bindCurrencyVM(viewModel: CurrencyVM): ViewModel


}