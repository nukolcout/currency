package com.example.currency.di.module

import android.app.Application
import android.content.Context
import com.example.currency.database.database_repository.UserDatabaseRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class
    ]
)
class AppModule {

    @Singleton
    @Provides
    fun provideContext(app: Application): Context = app.applicationContext


    @Provides
    fun provideDataEventHelper(): UserDatabaseRepository =
        UserDatabaseRepository()

}