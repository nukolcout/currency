package com.example.currency.di

import android.app.Application
import com.example.currency.TestTaskApplication
import com.example.currency.di.module.ApiModule
import com.example.currency.di.module.AppModule
import com.example.currency.di.module.MainActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ApiModule::class,
        MainActivityModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: TestTaskApplication)
}