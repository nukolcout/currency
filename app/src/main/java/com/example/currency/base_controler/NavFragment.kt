package com.example.currency.base_controler

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class NavFragment<V : ViewBinding> : Fragment() {

    protected lateinit var binding: V
    private var mainActivityUiController: MainActivityUiController? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflateViewBinding(inflater, container)
        return binding.root
    }

    fun setToolbar(id: Int, onClick: (() -> Unit)? = null) {
        mainActivityUiController?.updateBackButton(NavigationVisibility.HIDDEN)
        onClick?.run {
            mainActivityUiController?.updateBackButton(NavigationVisibility.VISIBLE)
            mainActivityUiController?.getView()?.setOnClickListener { invoke() }

        }
        mainActivityUiController?.setTitleToolbar(id)
    }

    protected abstract fun inflateViewBinding(inflater: LayoutInflater, container: ViewGroup?): V

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mainActivityUiController = context as? MainActivityUiController?
        } catch (e: Exception) {
            Log.e(NavFragment::class.simpleName, e.message.toString())
        }
    }
}