package com.example.currency.base_controler

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel :ViewModel(){
    val error = MutableLiveData<String>()

}