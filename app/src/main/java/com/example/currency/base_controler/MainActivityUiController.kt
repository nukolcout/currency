package com.example.currency.base_controler

import android.view.View

interface MainActivityUiController {
    fun updateBackButton(navigationVisibility: NavigationVisibility)
    fun setTitleToolbar(id: Int)
    fun getView(): View
}

enum class NavigationVisibility {
    VISIBLE,
    HIDDEN
}