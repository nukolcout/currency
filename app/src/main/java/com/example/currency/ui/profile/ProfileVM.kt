package com.example.currency.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.currency.base_controler.BaseViewModel
import com.example.currency.database.database_repository.UserDatabaseRepository
import com.example.currency.ui.base_adapter.CurrencyUI
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileVM @Inject constructor(private val db: UserDatabaseRepository) : BaseViewModel() {
    val currencyUI = MutableLiveData<ArrayList<CurrencyUI>>()

    fun getAllCurrency() {
        viewModelScope.launch {
            val currencyUi = arrayListOf<CurrencyUI>()
            val currencyDBs = db.getAll()
            for (itemDB in currencyDBs) {
                val currencyUI = CurrencyUI(currency = itemDB.currency, rate = itemDB.rate ?: 0.0)
                currencyUi.add(currencyUI)
            }
            currencyUI.postValue(currencyUi)
        }
    }

}