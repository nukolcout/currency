package com.example.currency.ui.currency

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.currency.base_controler.BaseViewModel
import com.example.currency.database.database_repository.UserDatabaseRepository
import com.example.currency.net.api.Api
import com.example.currency.ui.base_adapter.CurrencyUI
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class CurrencyVM @Inject constructor(private val api: Api, private val db: UserDatabaseRepository) :
    BaseViewModel() {
    val currencyResponse = MutableLiveData<ArrayList<CurrencyUI>>()
    private var disposable: Disposable? = null


    fun getCurrency() {
        disposable = api.getRepositories()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .subscribe({ response ->
                if (response.isSuccessful) {
                    response.body()?.run {
                        val currencyUi = this.rates.toCurrencyUi()
                        getCurrencyDB(currencyUi)
                    }
                } else {
                    error.postValue(response.message())
                }
            }, {
                error.postValue(it.message)
            })
    }

    fun addFavoriteCurrency(currencyUI: CurrencyUI) {
        db.insert(currencyUI.toCurrencyDB())
    }

    fun deleteFavoriteCurrency(currencyUI: CurrencyUI) {
        db.deleteById(currencyUI.currency)
    }

    private fun getCurrencyDB(currencyUi: ArrayList<CurrencyUI>) {
        viewModelScope.launch {
            val currencyDBs = db.getAll()
            for (itemDB in currencyDBs) {
                for (itemUI in currencyUi) {
                    if (itemDB.currency == itemUI.currency) {
                        itemUI.select = true
                    }
                }
            }
            currencyResponse.postValue(currencyUi)
        }
    }
}
