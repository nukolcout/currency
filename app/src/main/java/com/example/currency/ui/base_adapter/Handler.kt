package com.example.currency.ui.base_adapter

interface Handler {
    fun onClick(userInfo: CurrencyUI)
}