package com.example.currency.ui.base_adapter

import com.example.currency.database.model.CurrencyDB

data class CurrencyUI(val currency: String, val rate: Double, var select: Boolean = false) {
    fun toCurrencyDB(): CurrencyDB {
        val currencyDB = CurrencyDB()
        currencyDB.currency = currency
        currencyDB.rate = rate
        return currencyDB
    }
}