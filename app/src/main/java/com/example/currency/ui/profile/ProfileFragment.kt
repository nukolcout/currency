package com.example.currency.ui.profile

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.currency.R
import com.example.currency.base_controler.ViewModelFragment
import com.example.currency.databinding.FragmentProfileBinding
import com.example.currency.di.injectViewModel
import com.example.currency.ui.base_adapter.CurrencyAdapter

class ProfileFragment : ViewModelFragment<FragmentProfileBinding, ProfileVM>() {
    private val currencyAdapter = CurrencyAdapter()

    override fun createViewModel(): ProfileVM = injectViewModel(viewModelStore, viewModelFactory)

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileBinding = FragmentProfileBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbar(R.string.profile)
        initViewModel()
        setAdapter()
        intListener()
    }

    private fun intListener() {
        binding.button.setOnClickListener { findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToCurrencyFragment()) }
        binding.scroll.swipe.setOnRefreshListener { onRefresh() }
    }

    private fun onRefresh() {
        binding.scroll.swipe.isRefreshing = false
        binding.pbLoad.visibility=View.VISIBLE
       Handler(Looper.getMainLooper()).postDelayed(  {binding.pbLoad.visibility=View.GONE},5000)
    }

    private fun setAdapter() {
        binding.scroll.rvCurrency.adapter = currencyAdapter
    }

    private fun initViewModel() {
        with(viewModel) {
            currencyUI.observe(viewLifecycleOwner,{currencyAdapter.add(it)})
            getAllCurrency()
        }
    }
}