package com.example.currency.ui.base_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currency.databinding.ListItemCurrencyBinding

class CurrencyAdapter : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {
    var clickItem: ((idUser: CurrencyUI) -> Unit)? = null

    private var items = ArrayList<CurrencyUI>()

    fun add(currencyUIs: ArrayList<CurrencyUI>) {
        items.clear()
        items.addAll(currencyUIs)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemCurrencyBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val keysItem = items[position]
        holder.bind(keysItem)
    }

    inner class ViewHolder(private val binding: ListItemCurrencyBinding) :
        RecyclerView.ViewHolder(binding.root), Handler {
        fun bind(model: CurrencyUI) {
            binding.model = model
            binding.handler = this
            binding.executePendingBindings()
        }

        override fun onClick(userInfo: CurrencyUI) {
            clickItem?.run {
                userInfo.run { select = !select }
                val position = items.indexOf(userInfo)
                invoke(userInfo)
                notifyItemChanged(position)
            }
        }

    }

}