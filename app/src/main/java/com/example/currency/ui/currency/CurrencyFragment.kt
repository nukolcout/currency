package com.example.currency.ui.currency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.currency.R
import com.example.currency.base_controler.ViewModelFragment
import com.example.currency.databinding.FragmentCurrencyBinding
import com.example.currency.di.injectViewModel
import com.example.currency.ui.base_adapter.CurrencyAdapter
import com.example.currency.ui.base_adapter.CurrencyUI

class CurrencyFragment : ViewModelFragment<FragmentCurrencyBinding, CurrencyVM>() {
    private val currencyAdapter = CurrencyAdapter().apply { clickItem = { controlFavorite(it) } }

    private fun controlFavorite(currencyUI: CurrencyUI) {
        if (currencyUI.select) {
            viewModel.addFavoriteCurrency(currencyUI)
        } else {
            viewModel.deleteFavoriteCurrency(currencyUI)
        }
    }

    override fun createViewModel(): CurrencyVM = injectViewModel(viewModelStore, viewModelFactory)

    override fun inflateViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentCurrencyBinding =
        FragmentCurrencyBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvHistoryList.adapter = currencyAdapter
        initViewModel()
        setToolbar(R.string.all_currencies, onClick = { findNavController().popBackStack() })
    }


    private fun initViewModel() {
        with(viewModel) {
            currencyResponse.observe(viewLifecycleOwner, { addListCurrency(it) })
            viewModel.error.observe(viewLifecycleOwner, { showError(it) })
            getCurrency()
        }
    }

    private fun addListCurrency(it: ArrayList<CurrencyUI>) {
        currencyAdapter.add(it)
        binding.pbLoad.visibility = View.GONE
    }

    override fun showError(message: String?, ok: (() -> Unit)?) {
        super.showError(message, ok)
        binding.pbLoad.visibility = View.GONE
    }
}