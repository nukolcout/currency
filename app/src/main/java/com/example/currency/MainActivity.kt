package com.example.currency

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.currency.base_controler.MainActivityUiController
import com.example.currency.base_controler.NavigationVisibility
import com.example.currency.databinding.ActivityMainBinding
import com.example.currency.utils.hide
import com.example.currency.utils.show
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector, MainActivityUiController {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUi()
    }

    private fun initUi() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    override fun updateBackButton(navigationVisibility: NavigationVisibility) {
        when (navigationVisibility) {
            NavigationVisibility.VISIBLE -> {
                binding.toolbar.tvDone.show()
            }
            NavigationVisibility.HIDDEN -> {
                binding.toolbar.tvDone.hide()
            }
        }
    }

    override fun setTitleToolbar(id: Int) {
        binding.toolbar.tvTitle.text = getString(id)

    }

    override fun getView(): View = binding.toolbar.tvDone
}